﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotificationVKBot
{
    class AppConfig
    {
        private static readonly string TOKEN = "291717baa8d671684d738444f70807a616e91081fab2511ae23d8f6ab62e3bf6b45d30cbba2232be435e6";
        private static readonly ulong GROUPID = 100346873;
        private static Modes AppMode = Modes.Usualy;

        public static string GetToken() => TOKEN;
        public static ulong GetGroupID() => GROUPID;
        public static Modes GetAppMode() => AppMode;
        public static void SetAppMode(Modes mode) => AppMode = mode;

        public enum Modes
        {
            Usualy,
            AddNotifyDatetime,
            AddNotifyBody,
            SaveNotify,
            SelectMyNotifies
        }
    }
}
