﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotificationVKBot.DataBase
{
    class DBHelper
    {
        private static string CONNECTIONSTRING = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\dunae\source\repos\NotificationVKBot\NotificationVKBot\NotifyDB.mdf;Integrated Security=True;";

        public static string GetConnectionString() => CONNECTIONSTRING;

        /// <summary>
        /// Получить будущие напоминания определённого пользователя
        /// </summary>
        /// <param name="UserID">Уникальный идентификатор пользователя</param>
        /// <returns>Коллекция напоминаний</returns>
        public static List<Notify> GetNotifiesByUserID(long UserID)
        {
            List<Notify> list = new List<Notify>();
            string connectString = GetConnectionString();
            SqlConnection myConnection = new SqlConnection(connectString);
            myConnection.Open();
            string query = "Select * From Notifies where UserID = " + UserID + " AND NotifyDateTime > Cast('" + DateTime.Now + "' as datetime)";
            SqlCommand command = new SqlCommand(query, myConnection);
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                var nt = new Notify(reader.GetInt32(0), long.Parse(reader.GetInt32(1) + ""), reader.GetDateTime(2), reader.GetString(3));
                list.Add(nt);
            }
            myConnection.Close();
            return list;
        }

        /// <summary>
        /// Получить напоминание по его уникальному идентификатору
        /// </summary>
        /// <param name="ID">Уникальный идентификатор напоминания</param>
        /// <returns>Напоминание</returns>
        public static Notify GetNotifyByID(int ID)
        {
            Notify notify = null;
            string connectString = GetConnectionString();
            SqlConnection myConnection = new SqlConnection(connectString);
            myConnection.Open();
            string query = "Select * From Notifies where Id = " + ID;
            SqlCommand command = new SqlCommand(query, myConnection);
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                notify = new Notify(reader.GetInt32(0), reader.GetInt32(1), reader.GetDateTime(2), reader.GetString(3));
            }
            myConnection.Close();
            return notify;
        }

        /// <summary>
        /// Добавление в таблицу нового напоминания
        /// </summary>
        /// <param name="notify">Напоминание</param>
        public static void AddNewNotify(Notify notify) => NonExecuteQuery("INSERT INTO [Notifies] (UserID, NotifyDateTime, NotifyBody) Values (" + 
            notify.UserId + ", CAST ('" + 
            notify.NotifyDateTime + "' as datetime), N'" + 
            notify.NotifyBody + "')");

        /// <summary>
        /// Удаление напминания по его уникальному идентификатору
        /// </summary>
        /// <param name="ID">Уникальный идентификатор напоминания</param>
        public static void DeleteNotifyByID(int ID)
        {
            NonExecuteQuery("Delete from Notifies where Id = " + ID);
        }

        /// <summary>
        /// Удаление напоминаний, срок которых прошёл.
        /// </summary>
        /// <param name="UserID">Уникальный идентификатор пользователя.</param>
        public static void DeleteLastNotifiesByUserID(long UserID)
        {
            NonExecuteQuery("Delete from Notifies where UserId = " + UserID + " and NotifyDateTime < Cast('" + DateTime.Now + "' as datetime)");
        }

        /// <summary>
        /// Получить напоминание, которе самое близкое из будущих
        /// </summary>
        /// <returns>Напоминание</returns>
        public static Notify GetFollowNotify()
        {
            Notify notify = null;
            string connectString = GetConnectionString();
            SqlConnection myConnection = new SqlConnection(connectString);
            myConnection.Open();
            string query = "Select top(1) * From Notifies where NotifyDateTime > Cast('" + DateTime.Now + "' as datetime) order by NotifyDateTime";
            SqlCommand command = new SqlCommand(query, myConnection);
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                notify = new Notify(reader.GetInt32(0), reader.GetInt32(1), reader.GetDateTime(2), reader.GetString(3));
            }
            myConnection.Close();
            return notify;
        }

        /// <summary>
        /// Выполняет запрос к базе данных, который не нуждается в чтении ответа.
        /// </summary>
        /// <param name="query">Пользовательский запрос.</param>
        private static void NonExecuteQuery(string query)
        {
            string connectString = GetConnectionString();
            SqlConnection myConnection = new SqlConnection(connectString);
            myConnection.Open();
            SqlCommand command = new SqlCommand(query, myConnection);
            command.ExecuteNonQuery();
            myConnection.Close();
        }
    }
}
