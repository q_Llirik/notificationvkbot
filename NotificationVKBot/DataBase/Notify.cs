﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotificationVKBot.DataBase
{
    class Notify
    {
        public int? ID;
        public long UserId;
        public DateTime NotifyDateTime;
        public string NotifyBody;

        public Notify() { }

        public Notify(int id, long userid, DateTime notifydatetime, string notifybody)
        {
            ID = id;
            UserId = userid;
            NotifyDateTime = DateTime.Parse(notifydatetime.ToString("MM/dd/yyyy HH:mm"));
            NotifyBody = notifybody;
        }

        public Notify(long userid, DateTime notifydatetime, string notifybody)
        {
            UserId = userid;
            NotifyDateTime = DateTime.Parse(notifydatetime.ToString("MM/dd/yyyy HH:mm"));
            NotifyBody = notifybody;
        }
    }
}
