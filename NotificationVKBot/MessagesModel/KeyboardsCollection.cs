﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet.Enums.SafetyEnums;
using VkNet.Model;
using VkNet.Model.Keyboard;
using VkNet.Model.RequestParams;

namespace NotificationVKBot.MessagesModel
{
    class KeyboardsCollection
    {
        public static List<string> ButtonTitles => typeof(KeyboardsCollection).GetProperties().Where(w => w.PropertyType.FullName == typeof(String).FullName).ToList().Select(w => w.GetValue(w) as String).ToList();

        public static string AddNewNotifyTitle => "Добавить напоминание";
        public static string BackTitle => "Назад";
        public static string FurtherTitle => "Далее";
        public static string SaveTitle => "Сохранить";
        public static string DeleteNotifyTitle => "Удалить напоминания";
        public static string MyNotificationsTitle => "Мои напоминания";

        public static List<MessageKeyboardButton> ListButtons => typeof(KeyboardsCollection).GetFields().Select(w=>w.GetValue(w) as MessageKeyboardButton).ToList();

        public static MessageKeyboardButton AddNewNotifyKeyboard = new MessageKeyboardButton()
        {
            Color = KeyboardButtonColor.Primary,
            Action = new MessageKeyboardButtonAction() {Label = AddNewNotifyTitle }
        };

        public static MessageKeyboardButton BackKeyboard = new MessageKeyboardButton()
        {
            Color = KeyboardButtonColor.Negative,
            Action = new MessageKeyboardButtonAction() { Label = BackTitle }
        };

        public static MessageKeyboardButton SaveKeyboard = new MessageKeyboardButton()
        {
            Color = KeyboardButtonColor.Positive,
            Action = new MessageKeyboardButtonAction() { Label = SaveTitle }
        };

        public static MessageKeyboardButton MyNotificationsKeyboard = new MessageKeyboardButton()
        {
            Color = KeyboardButtonColor.Default,
            Action = new MessageKeyboardButtonAction() { Label = MyNotificationsTitle }
        };

        public static MessageKeyboardButton DeleteNotifyKeyboard = new MessageKeyboardButton()
        {
            Color = KeyboardButtonColor.Primary,
            Action = new MessageKeyboardButtonAction() { Label = DeleteNotifyTitle }
        };

        public static MessageKeyboardButton WithoutKeyboard = new MessageKeyboardButton() {
            Color = KeyboardButtonColor.Default,
            Action = new MessageKeyboardButtonAction() { Label = ""}
        };

        public static Dictionary<MessageKeyboardButton, Delegate> ButtonsWorking => new Dictionary<MessageKeyboardButton, Delegate>()
        {
            {AddNewNotifyKeyboard,  new ReceiveMessageDelegate(ProceduresForButton.AddNewNotifyAnswer)},
            {BackKeyboard,  new ReceiveMessageDelegate(ProceduresForButton.BackAnswer)},
            {SaveKeyboard,  new ReceiveMessageDelegate(ProceduresForButton.SaveAnswer)},
            {MyNotificationsKeyboard,  new ReceiveMessageDelegate(ProceduresForButton.MyNotificationsAnswer)},
            {WithoutKeyboard, new ReceiveMessageDelegate(ProceduresForButton.WithoutKeyboardMessagesAnswer)}
        };

        private delegate List<MessagesSendParams> ReceiveMessageDelegate(Message ReceiveMessage);
    }
}
