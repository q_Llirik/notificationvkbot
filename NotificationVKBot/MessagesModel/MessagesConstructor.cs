﻿using NotificationVKBot.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet.Model.Keyboard;
using VkNet.Model.RequestParams;

namespace NotificationVKBot.MessagesModel
{
    class MessagesConstructor
    {
        /// <summary>
        /// Создание отправляемого сообщения из данных
        /// </summary>
        /// <param name="MessageBody">Текст сообщения</param>
        /// <param name="UserID">Уникальный идентификатор пользователя</param>
        /// <param name="ButtonsList">Коллекция кнопок</param>
        /// <returns></returns>
        public static MessagesSendParams CreateMessage(string MessageBody, long UserID, List<MessageKeyboardButton> ButtonsList)
        {
            Random rnd = new Random();
            var resultKeyboard = new MessagesSendParams()
            {
                Message = MessageBody,
                RandomId = rnd.Next(),
                UserId = UserID,
                Keyboard = new MessageKeyboard
                {
                    OneTime = true,
                    Buttons = new List<List<MessageKeyboardButton>> { ButtonsList}
                }
            };

            return resultKeyboard;
        }
        /// <summary>
        /// Создание отправляемого сообщения из данных
        /// </summary>
        /// <param name="MessageBody">Текст сообщения</param>
        /// <param name="UserID">Уникальный идентификатор пользователя</param>
        /// <returns></returns>
        public static MessagesSendParams CreateMessage(string MessageBody, long UserID)
        {
            Random rnd = new Random();
            return new MessagesSendParams()
            {
                Message = MessageBody,
                RandomId = rnd.Next(),
                UserId = UserID
            };
        }

        /// <summary>
        /// Экземпляр вывода напоминания.
        /// </summary>
        /// <param name="notify">Напоминание</param>
        /// <returns>Напоминание в виде строки</returns>
        public static string NotifyToString(Notify notify)
        {
            string result = "Вы просили напоминть о слудующем: " + 
                notify.NotifyBody + " - в " + 
                notify.NotifyDateTime.ToShortTimeString() + " " +
                notify.NotifyDateTime.ToShortDateString();
            
            if (notify.ID.HasValue)
            {
                result += '\n' + "ID = " + notify.ID;
            }

            return result;
        }
    }
}
