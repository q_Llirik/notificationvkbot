﻿using NotificationVKBot.DataBase;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet.Model;
using VkNet.Model.Keyboard;
using VkNet.Model.RequestParams;

namespace NotificationVKBot.MessagesModel
{
    class ProceduresForButton
    {
        private static Notify saveNotify = new Notify();

        public static List<MessagesSendParams> AddNewNotifyAnswer(Message ReceiveMessage)
        {
            AppConfig.SetAppMode(AppConfig.Modes.AddNotifyBody);
            return new List<MessagesSendParams>{ MessagesConstructor.CreateMessage("Введите о чём вам напоминть.", ReceiveMessage.UserId.Value, new List<MessageKeyboardButton>() {
                    KeyboardsCollection.BackKeyboard
                }) };
        }

        public static List<MessagesSendParams> BackAnswer(Message ReceiveMessage)
        {
            AppConfig.SetAppMode(AppConfig.Modes.Usualy);
            return WithoutKeyboardMessagesAnswer(ReceiveMessage);
        }

        public static List<MessagesSendParams> SaveAnswer(Message ReceiveMessage)
        {
            if (saveNotify.NotifyDateTime > DateTime.Now)
            { 
                DBHelper.AddNewNotify(saveNotify);
                DBHelper.DeleteLastNotifiesByUserID(ReceiveMessage.UserId.Value);

                AppConfig.SetAppMode(AppConfig.Modes.Usualy);
                return new List<MessagesSendParams>{ MessagesConstructor.CreateMessage("Ваше напоминание сохранено. ", ReceiveMessage.UserId.Value, new List<MessageKeyboardButton>()
                            {
                                KeyboardsCollection.BackKeyboard
                            }) };
            }
            else
            {
                var list = WithoutKeyboardMessagesAnswer(ReceiveMessage);
                list.Add(MessagesConstructor.CreateMessage("Время вашего напоминания вышло.", ReceiveMessage.UserId.Value, new List<MessageKeyboardButton>()
                            {
                                KeyboardsCollection.BackKeyboard
                            }));
                AppConfig.SetAppMode(AppConfig.Modes.AddNotifyDatetime);
                return list;
            }
        }

        public static List<MessagesSendParams> MyNotificationsAnswer(Message ReceiveMessage)
        {
            var message = "";
            var notifies = DBHelper.GetNotifiesByUserID(ReceiveMessage.UserId.Value);
            if (notifies.Count == 0)
            {
                message += "У вас нет напоминаний.";
            }
            else
            {
                message += "У вас есть следующие напоминания:" + '\n';
                foreach(var i in notifies)
                {
                    message += MessagesConstructor.NotifyToString(i) + '\n';
                }
                message += "Если хотите удалить, то отправьте ID напоминания.";
                AppConfig.SetAppMode(AppConfig.Modes.SelectMyNotifies);
            }
            return new List<MessagesSendParams>{ MessagesConstructor.CreateMessage(message, ReceiveMessage.UserId.Value, new List<MessageKeyboardButton>() {
                KeyboardsCollection.BackKeyboard
            }) };
        }

        public static List<MessagesSendParams> WithoutKeyboardMessagesAnswer(Message ReceiveMessage)
        {
            switch(AppConfig.GetAppMode())
            {
                case AppConfig.Modes.Usualy:
                    {
                        return new List<MessagesSendParams>{ MessagesConstructor.CreateMessage("Слушаю Вас", ReceiveMessage.UserId.Value, new List<MessageKeyboardButton>()
                        {
                            KeyboardsCollection.AddNewNotifyKeyboard,
                            KeyboardsCollection.MyNotificationsKeyboard
                        }) };
                    }
                case AppConfig.Modes.AddNotifyBody:
                    {
                        if (ReceiveMessage.Body.Trim(' ').Length != 0)
                        {
                            saveNotify.NotifyBody = ReceiveMessage.Body;
                            saveNotify.UserId = ReceiveMessage.UserId.Value;
                            AppConfig.SetAppMode(AppConfig.Modes.AddNotifyDatetime);
                            return new List<MessagesSendParams>{ MessagesConstructor.CreateMessage("Введите дату и время напоминания." + '\n' + "P.s. Рекомендуемый формат: ДД.ММ.ГГ ЧЧ:ММ:СС", ReceiveMessage.UserId.Value, new List<MessageKeyboardButton>() {
                                KeyboardsCollection.BackKeyboard
                            }) };
                        }
                        else
                        {
                            return new List<MessagesSendParams>{ MessagesConstructor.CreateMessage("Сообщение напоминания не может быть пустым.", ReceiveMessage.UserId.Value, new List<MessageKeyboardButton>() {
                                KeyboardsCollection.BackKeyboard
                            }) };
                        }
                    }
                case AppConfig.Modes.AddNotifyDatetime:
                    {
                        DateTime dt = DateTime.Now;
                        if (DateTime.TryParse(ReceiveMessage.Body, out dt))
                        {
                            if (dt <= DateTime.Now)
                            {
                                return new List<MessagesSendParams>{ MessagesConstructor.CreateMessage("Дата напоминания должна быть больше сегоднешней, повторите ввод." + '\n' + "P.s. Рекомендуемый формат: ДД.ММ.ГГ ЧЧ:ММ:СС", ReceiveMessage.UserId.Value, new List<MessageKeyboardButton>() {
                                KeyboardsCollection.BackKeyboard
                                }) };
                            }
                            else
                            {
                                saveNotify.NotifyDateTime = dt;
                                AppConfig.SetAppMode(AppConfig.Modes.SaveNotify);
                                return new List<MessagesSendParams>{ MessagesConstructor.CreateMessage("Вот ваше напоминание: " + '\n' + MessagesConstructor.NotifyToString(saveNotify) + '\n' + "Нажмите Сохранить, если всё правильно. Если нет, то Назад.",
                                    ReceiveMessage.UserId.Value,
                                    new List<MessageKeyboardButton>() {
                                    KeyboardsCollection.SaveKeyboard,
                                    KeyboardsCollection.BackKeyboard
                                        }) };
                            }
                        }
                        else
                        {
                            return new List<MessagesSendParams>{ MessagesConstructor.CreateMessage("Введённая вами дата непрваильна, повторите ввод." + '\n' + "P.s. Рекомендуемый формат: ДД.ММ.ГГ ЧЧ:ММ:СС", ReceiveMessage.UserId.Value, new List<MessageKeyboardButton>() {
                                KeyboardsCollection.BackKeyboard
                            }) };
                        }
                    }
                case AppConfig.Modes.SelectMyNotifies:
                    {
                        var id = 0;
                        if (!int.TryParse(ReceiveMessage.Body, out id))
                        {
                            return new List<MessagesSendParams>{ MessagesConstructor.CreateMessage("Я не могу понять вас. Повторите ввод.", ReceiveMessage.UserId.Value, new List<MessageKeyboardButton>() {
                                KeyboardsCollection.BackKeyboard
                            }) };
                        }
                        else
                        {
                            var saveNotify = DBHelper.GetNotifyByID(id);
                            if (saveNotify == null)
                            {
                                return new List<MessagesSendParams>{
                                    MessagesConstructor.CreateMessage("У вас нет напоминания с таким ID. Потворите ввод.", ReceiveMessage.UserId.Value, new List<MessageKeyboardButton>() {
                                    KeyboardsCollection.BackKeyboard
                                }) };
                            }
                            else
                            {
                                if (saveNotify.UserId != ReceiveMessage.UserId.Value)
                                {
                                    return new List<MessagesSendParams>{ MessagesConstructor.CreateMessage("Я не могу понять вас. Повторите ввод.", ReceiveMessage.UserId.Value, new List<MessageKeyboardButton>() {
                                        KeyboardsCollection.BackKeyboard
                                    }) };
                                }
                                else
                                {
                                    DBHelper.DeleteNotifyByID(id);
                                    AppConfig.SetAppMode(AppConfig.Modes.Usualy);
                                    return new List<MessagesSendParams>{ MessagesConstructor.CreateMessage("Удаление прошло успешно. " + '\n' + "Слушаю Вас", ReceiveMessage.UserId.Value, new List<MessageKeyboardButton>()
                                    {
                                        KeyboardsCollection.AddNewNotifyKeyboard,
                                        KeyboardsCollection.MyNotificationsKeyboard
                                    }) };
                                }
                            }
                        }
                    }
                default:
                    {
                        var i = Program.vkApi.Messages.GetHistory(new MessagesGetHistoryParams
                        {
                            GroupId = AppConfig.GetGroupID(),
                            UserId = ReceiveMessage.UserId.Value,
                            Count = 10,
                            Extended = true,
                        }).Messages.Where(w=>w.FromId < 0).First();
                        return new List<MessagesSendParams> { MessagesConstructor.CreateMessage(i.Text, ReceiveMessage.UserId.Value, i.Keyboard.Buttons.First().ToList()) };
                    }
            }
        }
    }
}
