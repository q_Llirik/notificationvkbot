﻿using NotificationVKBot.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet.Model;
using VkNet.Model.Keyboard;
using VkNet.Model.RequestParams;

namespace NotificationVKBot.MessagesModel
{
    class SelectSendMessage
    {
        static public List<MessagesSendParams> GetSendMessageFromReceiveMessage(Message ReceiveMessage)
        {
            if (!KeyboardsCollection.ButtonTitles.Contains(ReceiveMessage.Body) && AppConfig.GetAppMode() == AppConfig.Modes.Usualy)
            {
                return (List<MessagesSendParams>)KeyboardsCollection.ButtonsWorking[KeyboardsCollection.WithoutKeyboard].DynamicInvoke(new object[] { ReceiveMessage });
            }
            
            var key = KeyboardsCollection.ListButtons.FirstOrDefault(w => w.Action.Label == ReceiveMessage.Body);            
            return (List<MessagesSendParams>)KeyboardsCollection.ButtonsWorking[key != null ? key : KeyboardsCollection.WithoutKeyboard].DynamicInvoke(new object[] { ReceiveMessage });
        }
    }
}
