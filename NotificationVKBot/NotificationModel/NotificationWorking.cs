﻿using NotificationVKBot.DataBase;
using NotificationVKBot.MessagesModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VkNet.Model.Keyboard;

namespace NotificationVKBot.NotificationModel
{
    class NotificationWorking
    {
        private static Notify notify = null;

        public static void StartFollowTheNotify()
        {
            var timer = new System.Timers.Timer(1000);
            timer.AutoReset = true;
            timer.Elapsed += Timer_Elapsed;
            timer.Enabled = true;
        }

        private static void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (notify == null)
            {
                notify = DBHelper.GetFollowNotify();
            }

            if (notify == null)
                return;

            if (notify.NotifyDateTime.Date == DateTime.Now.Date && notify.NotifyDateTime.Hour == DateTime.Now.Hour && notify.NotifyDateTime.Minute == DateTime.Now.Minute && notify.NotifyDateTime.Second == DateTime.Now.Second)
            {
                DBHelper.DeleteNotifyByID(notify.ID.Value);
                DBHelper.DeleteLastNotifiesByUserID(notify.UserId);

                Program.vkApi.Messages.Send(MessagesConstructor.CreateMessage("Вы просили напомнить о следующем: " + notify.NotifyBody, notify.UserId));
                notify = null;
            }
        }
    }
}
