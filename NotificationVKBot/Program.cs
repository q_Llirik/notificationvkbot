﻿using NotificationVKBot.NotificationModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Enums.SafetyEnums;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace NotificationVKBot
{
    class Program
    {
        public static VkApi vkApi = new VkApi();
        static void Main(string[] args)
        {
            var i = typeof(MessagesModel.KeyboardsCollection).GetFields();
            vkApi.Authorize(new ApiAuthParams() { AccessToken = AppConfig.GetToken() });

            NotificationWorking.StartFollowTheNotify();
            while (true)
            {
                var s = vkApi.Groups.GetLongPollServer(AppConfig.GetGroupID());
                var poll = vkApi.Groups.GetBotsLongPollHistory(
                                      new BotsLongPollHistoryParams()
                                      {
                                          Server = s.Server,
                                          Ts = s.Ts,
                                          Key = s.Key,
                                          Wait = 10
                                      });


                if (poll?.Updates == null) continue;

                foreach (var update in poll.Updates)
                {
                    if (update.Type == GroupUpdateType.MessageNew)
                    {
                        var user = vkApi.Users.Get(new List<long> { update.Message.UserId.Value }).First();
                        Console.WriteLine("{0}--User {1}({2}): {3} ", update.Message.Date, user.FirstName + ' ' + user.LastName, user.Id, update.Message.Body);

                        foreach(var message in MessagesModel.SelectSendMessage.GetSendMessageFromReceiveMessage(update.Message))
                            vkApi.Messages.Send(message);
                    }
                }
            }
        }
    }
}
